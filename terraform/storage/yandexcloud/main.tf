resource "yandex_iam_service_account" "sa" {
  folder_id = var.folder_id
  name      = "tf-sa"
}

resource "yandex_resourcemanager_folder_iam_member" "sa-editor" {
  folder_id = var.folder_id
  role      = "storage.editor"
  member    = "serviceAccount:${yandex_iam_service_account.sa.id}"
}

resource "yandex_iam_service_account_static_access_key" "sa-static-key" {
  service_account_id = yandex_iam_service_account.sa.id
  description        = "static access key for object storage"
}

resource "yandex_storage_bucket" "bucket" {
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket = var.name

  acl = "public-read"
}

resource "yandex_storage_object" "object" {
  for_each = var.objects

  bucket = yandex_storage_bucket.bucket.id

  key          = each.key
  source       = each.value.source
  content_type = each.value.content_type

  acl = "public-read"
}
