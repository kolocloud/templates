output "public_link" {
  value = [
    for item in yandex_storage_object.object : "https://${yandex_storage_bucket.bucket.bucket_domain_name}/${item.key}"
  ]
}
