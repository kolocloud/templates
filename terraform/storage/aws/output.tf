output "public_link" {
  value = [
    for item in aws_s3_object.object : "https://${aws_s3_bucket.my_bucket.bucket_domain_name}/${item.key}"
  ]
}
