resource "aws_s3_bucket" "my_bucket" {
  bucket = var.name
}

resource "aws_s3_bucket_acl" "example" {
  depends_on = [aws_s3_bucket_ownership_controls.example]

  bucket = aws_s3_bucket.my_bucket.id
  acl    = "private"
}

resource "aws_s3_bucket_ownership_controls" "example" {
  bucket = aws_s3_bucket.my_bucket.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_public_access_block" "example" {
  bucket = aws_s3_bucket.my_bucket.id

  block_public_acls   = false
  block_public_policy = false
}

resource "aws_s3_object" "object" {
  depends_on = [aws_s3_bucket_acl.example]

  for_each = var.objects

  bucket = aws_s3_bucket.my_bucket.id

  key          = each.key
  source       = each.value.source
  content_type = each.value.content_type

  acl = "public-read"
}
