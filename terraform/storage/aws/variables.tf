variable "name" {
  type = string
}

variable "region" {
  type = string
}

variable "access_key" {
  type = string
  sensitive = true
}

variable "secret_key" {
  type = string
  sensitive = true
}

variable "objects" {
  type    = map(object({
    source = string
    content_type = string
  }))
}
