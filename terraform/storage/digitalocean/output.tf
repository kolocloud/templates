output "public_link" {
  value = [
    for item in digitalocean_spaces_bucket_object.object : "https://${digitalocean_spaces_bucket.bucket.bucket_domain_name}/${item.key}"
  ]
}
