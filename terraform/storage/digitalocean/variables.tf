variable "token" {
  type = string
  sensitive = true
}

variable "spaces_access_id" {
  type = string
  sensitive = true
}

variable "spaces_secret_key" {
  type = string
  sensitive = true
}

variable "region" {
  type = string
}

variable "name" {
  type = string
}

variable "objects" {
  type    = map(object({
    source = string
    content_type = string
  }))
}
