resource "digitalocean_spaces_bucket" "bucket" {
  name   = var.name
  region = var.region
}

resource "digitalocean_spaces_bucket_object" "object" {
  for_each = var.objects

  region       = var.region
  bucket       = digitalocean_spaces_bucket.bucket.name

  key          = each.key
  source       = each.value.source
  content_type = each.value.content_type

  acl          = "public-read"
}
