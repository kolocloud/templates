resource "google_storage_bucket" "bucket" {
  name     = var.name
  location = var.region
  project = var.project
}

resource "google_storage_bucket_access_control" "public_rule" {
  bucket = google_storage_bucket.bucket.name
  role   = "READER"
  entity = "allUsers"
}

resource "google_storage_object_access_control" "public_rule_objects" {
  for_each = var.objects

  object = each.key
  bucket = google_storage_bucket.bucket.name
  role   = "READER"
  entity = "allUsers"

  depends_on = [google_storage_bucket_object.object]
}

resource "google_storage_bucket_object" "object" {
  for_each = var.objects

  bucket = google_storage_bucket.bucket.name

  name         = each.key
  source      = each.value.source
  content_type = each.value.content_type
}
