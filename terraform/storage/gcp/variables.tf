variable "name" {
  type = string
}

variable "region" {
  type = string
}

variable "zone" {
  type = string
}

variable "project" {
  type = string
}

variable "objects" {
  type    = map(object({
    source = string
    content_type = string
  }))
}
