output "public_link" {
  value = [
    for item in google_storage_bucket_object.object : "https://storage.googleapis.com/${google_storage_bucket.bucket.name}/${item.name}"
  ]
}
