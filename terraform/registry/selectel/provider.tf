terraform {
  backend "http" {}

  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.35.0"
    }
    selectel = {
      source  = "selectel/selectel"
      version = "~> 3.6.2"
    }
  }
}

provider "openstack" {
  auth_url    = "https://api.selvpc.ru/identity/v3"
  domain_name = var.domain_name
  tenant_id   = var.tenant_id
  user_name   = var.user_name
  password    = var.password
  region      = var.region
}

provider "selectel" {
  token = var.token
}
