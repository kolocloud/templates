variable "token" {
  type = string
  sensitive = true
}

variable "domain_name" {
  type = string
  sensitive = true
}

variable "tenant_id" {
  type = string
  sensitive = true
}

variable "user_name" {
  type = string
  sensitive = true
}

variable "password" {
  type = string
  sensitive = true
}

variable "region" {
  default = "ru-3"
}

variable "az_zone" {
  default = "ru-3b"
}
