resource "yandex_container_registry" "default" {
  name      = var.name
  folder_id = var.folder_id

  labels = {
    my-label = "my-label-value"
  }
}
