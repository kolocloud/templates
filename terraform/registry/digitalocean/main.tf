resource "digitalocean_container_registry" "default" {
  name                   = var.name
  subscription_tier_slug = "starter"
  region = var.region
}
