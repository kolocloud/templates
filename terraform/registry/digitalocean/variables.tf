variable "token" {
  type = string
  sensitive = true
}

variable "region" {
  type = string
}

variable "name" {
  type = string
}

