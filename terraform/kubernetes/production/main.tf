resource "kubernetes_namespace" "production" {
  metadata {
    name = "production"
  }
}

module "service-account" {
  source    = "./modules/service-account"
  domain = var.domain

  depends_on = [
    kubernetes_namespace.production
  ]
}
