variable "kubernetes_config_file" {
  type = string
  sensitive = true
}

variable "domain" {
  type = string
}
