terraform {
  backend "http" {}
}

provider "kubernetes" {
  config_path = var.kubernetes_config_file
}
