resource "kubernetes_service_account" "production-service-account" {
  metadata {
    name = "production-service-account"
    namespace = "production"
  }
}

resource "kubernetes_secret" "production-service-account-token" {
  metadata {
    name = "production-service-account-token"
    namespace = "production"

    annotations = {
      "kubernetes.io/service-account.name" = "production-service-account"
    }
  }

  type = "kubernetes.io/service-account-token"

  depends_on = [
    kubernetes_service_account.production-service-account
  ]
}

resource "kubernetes_role" "production-role" {
  metadata {
    name = "production-role"
    namespace = "production"
  }

  rule {
    api_groups     = ["*"]
    resources      = ["*"]
    verbs          = ["*"]
  }
}

resource "kubernetes_role_binding" "production-rolebinding" {
  metadata {
    name      = "production-rolebinding"
    namespace = "production"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "production-role"
  }
  subject {
    kind      = "ServiceAccount"
    namespace = "production"
    name      = "production-service-account"
  }

  depends_on = [
    kubernetes_service_account.production-service-account,
    kubernetes_role.production-role
  ]
}

resource "local_sensitive_file" "k3s-app" {
  content = <<EOF
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ${base64encode(kubernetes_secret.production-service-account-token.data["ca.crt"])}
    server: https://kubernetes.${var.domain}:6443
  name: cluster
contexts:
- context:
    cluster: cluster
    user: user
    namespace: production
  name: context
current-context: context
kind: Config
preferences: {}
users:
- name: user
  user:
    token: ${kubernetes_secret.production-service-account-token.data.token}
EOF

  filename = "production-service-account.yaml"

  depends_on = [
    kubernetes_role_binding.production-rolebinding
  ]
}
