terraform {
  backend "http" {}

  required_providers {
    google = {
      source = "hashicorp/google"
      version = "5.5.0"
    }
    google-beta = {
      source = "hashicorp/google-beta"
      version = "5.5.0"
    }
  }
}

provider "google" {
  credentials = file("credentials/service-account.json")

  project = var.project
  region  = var.region
  zone    = var.zone
}

provider "google-beta" {
  credentials = file("credentials/service-account.json")

  project = var.project
  region  = var.region
  zone    = var.zone
}
