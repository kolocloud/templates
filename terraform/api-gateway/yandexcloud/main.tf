resource "yandex_api_gateway" "test-api-gateway" {
  name = "my-gateway"
  description = "any description"
  labels = {
    label       = "label"
    empty-label = ""
  }

  custom_domains {
    fqdn = "gw.${var.domain}"
    certificate_id = yandex_cm_certificate.emerald.id
  }

  spec = file("spec/openapi.yaml")
}

resource "yandex_cm_certificate" "emerald" {
  name    = "cloud-emerald"

  self_managed {
    certificate = file("cert/fullchain.pem")
    private_key = file("cert/privkey.pem")
  }
}

resource "yandex_dns_zone" "gateway" {
  name        = "gateway"
  description = "gateway"

  labels = {
    label1 = "gateway"
  }

  zone             = "${var.domain}."
  public           = true
}

resource "yandex_dns_recordset" "gateway" {
  name    = "gw.${var.domain}."
  zone_id = yandex_dns_zone.gateway.id
  type    = "ANAME"
  ttl     = 60
  data    = [yandex_api_gateway.test-api-gateway.domain]
}
