variable "name" {
  type = string
}

variable "cloud_id" {
  type = string
}

variable "folder_id" {
  type = string
}

variable "zone" {
  type = string
}

variable "token" {
  type = string
  sensitive = true
}

variable "storage_access_key" {
  type = string
  sensitive = true
}

variable "storage_secret_key" {
  type = string
  sensitive = true
}

variable "domain" {
  type = string
}
