variable "region" {
  type = string
}

variable "domain" {
  type = string
  default = "example.com"
}

variable "access_key" {
  type = string
  sensitive = true
}

variable "secret_key" {
  type = string
  sensitive = true
}
