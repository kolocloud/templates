output "name_servers" {
  value = aws_route53_zone.prod-zone.name_servers
}

output "zone_id" {
  value = aws_route53_zone.prod-zone.zone_id
}
