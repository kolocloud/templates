variable "domain" {
  type = string
}

variable "network_ids" {
  type = list(string)
}

variable "target_ips" {
  type = list(string)
}

variable "subdomains" {
  type = list(string)
}
