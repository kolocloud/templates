resource "yandex_dns_zone" "gateway" {
  name        = "gateway"
  description = "gateway"

  labels = {
    label1 = "gateway"
  }

  zone             = "${var.domain}."
  public           = true
  private_networks = var.network_ids
}

resource "yandex_dns_recordset" "a" {
  for_each = toset(var.subdomains)
  name    = "${each.value}.${var.domain}."
  zone_id = yandex_dns_zone.gateway.id
  type    = "A"
  ttl     = 60
  data    = var.target_ips
}
