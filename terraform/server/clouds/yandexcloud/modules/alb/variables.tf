variable "domain" {
  type = string
}

variable "zone" {
  type = string
}

variable "instance_ip" {
  type = string
}

variable "network_id" {
  type = string
}

variable "subnet_ids" {
  type = list(string)
}

variable "folder_id" {
  type = string
}

variable "ssh_key" {
  type = string
  sensitive = true
}
