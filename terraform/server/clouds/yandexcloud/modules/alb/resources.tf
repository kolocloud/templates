resource "yandex_cm_certificate" "cert" {
  name    = "cert"

  self_managed {
    certificate = file("cert/fullchain.pem")
    private_key = file("cert/privkey.pem")
  }
}

resource "yandex_alb_target_group" "foo" {
  name      = "my-target-group"

  target {
    subnet_id = var.subnet_ids[0]
    ip_address   = var.instance_ip
  }
}

resource "yandex_alb_backend_group" "alb-bg" {
  name                     = "alb-bg"

  http_backend {
    name                   = "backend-1"
    port                   = 80
    target_group_ids       = [yandex_alb_target_group.foo.id]
    healthcheck {
      timeout              = "10s"
      interval             = "2s"
      healthcheck_port     = 80
      stream_healthcheck {

      }
    }
  }
}

resource "yandex_alb_http_router" "alb-router" {
  name   = "alb-router"
}

resource "yandex_alb_virtual_host" "alb-host" {
  name           = "alb-host"
  http_router_id = yandex_alb_http_router.alb-router.id
  authority      = ["*.${var.domain}"]
  route {
    name = "route-1"
    http_route {
      http_route_action {
        backend_group_id = yandex_alb_backend_group.alb-bg.id
      }
    }
  }
}

resource "yandex_alb_load_balancer" "alb-1" {
  name               = "alb-1"
  network_id         = var.network_id

  allocation_policy {
    location {
      zone_id   = var.zone
      subnet_id = var.subnet_ids[0]
    }
  }

  listener {
    name = "alb-listener"

    endpoint {
      address {
        external_ipv4_address {
        }
      }
      ports = [ 443 ]
    }

    tls {
      default_handler {
        certificate_ids = [yandex_cm_certificate.cert.id]
        http_handler {
          http_router_id = yandex_alb_http_router.alb-router.id
        }
      }
    }
  }
}
