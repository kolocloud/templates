variable "domain" {
  type = string
}

variable "cores" {
  type = string
}

variable "memory" {
  type = string
}

variable "size" {
  type = string
}

variable "zone" {
  type = string
}

variable "ssh_key" {
  type = string
  sensitive = true
}

variable "prefix" {
  type = string
  default = "terraform-cluster"
}
