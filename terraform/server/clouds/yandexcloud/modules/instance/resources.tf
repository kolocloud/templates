resource "yandex_compute_instance" "vm-1" {
  name = "kubernetes"
  allow_stopping_for_update = true

  resources {
    cores  = var.cores
    memory = var.memory
  }

  boot_disk {
    initialize_params {
      image_id = "fd89boblh6d5vruo39lm"
      size     = var.size
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  scheduling_policy {
    preemptible = true
  }

  metadata = {
    ssh-keys = "${var.ssh_key}"
  }
}

resource "yandex_vpc_network" "network-1" {
  name = "${var.prefix}-network1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "${var.prefix}-subnet1"
  zone           = var.zone
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}
