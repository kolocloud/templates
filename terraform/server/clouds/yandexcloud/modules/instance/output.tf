output "network_id" {
  value = yandex_vpc_network.network-1.id
}

output "subnet_id" {
  value = yandex_vpc_subnet.subnet-1.id
}

output "cluster_ip" {
  value = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
}

output "cluster_ip_private" {
  value = yandex_compute_instance.vm-1.network_interface.0.ip_address
}
