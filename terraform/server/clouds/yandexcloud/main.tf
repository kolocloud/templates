module "instance" {
  source    = "./modules/instance"
  domain = var.domain
  zone = var.zone
  size = var.size

  cores = var.cores
  memory = var.memory
  ssh_key = var.ssh_key
  prefix = var.prefix
}

module "alb" {
  count = var.alb_enable ? 1 : 0

  source    = "./modules/alb"
  domain = var.domain

  network_id = module.instance.network_id
  subnet_ids = [module.instance.subnet_id]
  folder_id = var.folder_id
  ssh_key = var.ssh_key
  zone = var.zone
  instance_ip = module.instance.cluster_ip_private

  depends_on = [
    module.instance
  ]
}

module "dns-instance" {
  count = var.alb_enable ? 0 : 1

  source    = "./modules/dns"
  domain = var.domain
  subdomains = var.subdomains

  target_ips = [module.instance.cluster_ip]
  network_ids = [module.instance.network_id]

  depends_on = [
    module.instance
  ]
}

module "dns-alb" {
  count = var.alb_enable ? 1 : 0

  source    = "./modules/dns"
  domain = var.domain
  subdomains = var.subdomains

  target_ips = [module.alb[0].ip_public]
  network_ids = [module.instance.network_id]

  depends_on = [
    module.alb
  ]
}

module "k3s" {
  source    = "../../modules/k3s"
  ssh_user = var.ssh_user

  file_key_private = var.file_key_private
  kubernetes_config_file = var.kubernetes_config_file

  ip_public = module.instance.cluster_ip
  ip_private = module.instance.cluster_ip_private

  k3s_config_file = var.k3s_config_file

  depends_on = [
    module.instance
  ]
}
