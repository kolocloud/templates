output "external_ip" {
  value = module.instance.cluster_ip
}

output "lb_external_ip" {
  value = [
    for item in module.alb : item.ip_public
  ]
}
