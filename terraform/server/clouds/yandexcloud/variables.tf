variable "alb_enable" {
  type = bool
}

variable "cloud_id" {
  type = string
  sensitive = true
}

variable "folder_id" {
  type = string
  sensitive = true
}

variable "token" {
  type = string
  sensitive = true
}

variable "zone" {
  type = string
}

variable "cores" {
  type = string
}

variable "memory" {
  type = string
}

variable "size" {
  type = string
}

variable "file_key_private" {
  type = string
}

variable "domain" {
  type = string
  default = "example.com"
}

variable "subdomains" {
  type = list(string)
  default = ["*"]
}

variable "kubernetes_config_file" {
  type = string
  sensitive = true
}

variable "k3s_config_file" {
  type = string
}

variable "ssh_key" {
  type = string
}

variable "ssh_user" {
  type = string
}

variable "prefix" {
  type = string
  default = "terraform-cluster"
}
