module "instance" {
  source    = "./modules/instance"
  domain = var.domain
  region = var.region
  size = var.size
  file_key_private = var.file_key_private
  public_key = var.public_key
  prefix = var.prefix
}

module "elb" {
  count = var.elb_enable ? 1 : 0

  source    = "./modules/elb"
  domain = var.domain
  zone_id = var.zone_id

  instance_id = module.instance.cluster_id

  depends_on = [
    module.instance
  ]
}

module "dns-elb" {
  count = var.elb_enable ? 1 : 0

  source    = "./modules/dns"
  domain = var.domain
  subdomains = var.subdomains
  zone_id = var.zone_id

  target_ips = module.elb[0].ips

  depends_on = [
    module.elb
  ]
}

module "dns-instance" {
  count = !var.elb_enable ? 1 : 0

  source    = "./modules/dns"
  domain = var.domain
  subdomains = var.subdomains
  zone_id = var.zone_id

  target_ips = [module.instance.cluster_ip]

  depends_on = [
    module.instance
  ]
}

module "k3s" {
  source    = "../../modules/k3s"
  ssh_user = var.ssh_user

  file_key_private = var.file_key_private
  kubernetes_config_file = var.kubernetes_config_file
  k3s_config_file = var.k3s_config_file

  ip_public = module.instance.cluster_ip
  ip_private = module.instance.cluster_ip_private

  depends_on = [
    module.instance
  ]
}
