file_key_private = "~/.ssh/id_rsa_cloud"
kubernetes_config_file = "~/.k3s/config"

k3s_config_file = "/home/ec2-user/k3s.yaml"

region = "us-east-1"
size = "t2.micro"
zone_id = "Z08947863I4RSDK06HMY5"

ssh_user = "ec2-user"

elb_enable = false
