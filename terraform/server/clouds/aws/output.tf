output "external_ip" {
  value = module.instance.cluster_ip
}

output "loadbalancer_ips" {
  value = [
    for item in module.elb : item.ips
  ]
}
