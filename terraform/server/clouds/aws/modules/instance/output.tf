output "cluster_id" {
  value = aws_instance.app_server.id
}

output "cluster_ip" {
  value = aws_instance.app_server.public_ip
}

output "cluster_ip_private" {
  value = aws_instance.app_server.private_ip
}
