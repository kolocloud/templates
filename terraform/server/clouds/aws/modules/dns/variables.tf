variable "domain" {
  type = string
}

variable "zone_id" {
  type = string
}

variable "target_ips" {
  type = list(string)
}

variable "subdomains" {
  type = list(string)
}
