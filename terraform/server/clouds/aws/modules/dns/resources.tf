resource "aws_route53_record" "domains" {
  zone_id = var.zone_id
  for_each = toset(var.subdomains)
  name    = "${each.value}.${var.domain}"
  type    = "A"
  ttl     = 30
  records = var.target_ips
}
