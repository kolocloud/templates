data "dns_a_record_set" "elb" {
  host = aws_elb.main.dns_name
}

output "ips" {
  value = data.dns_a_record_set.elb.addrs
}
