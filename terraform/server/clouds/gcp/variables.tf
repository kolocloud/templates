variable "dns_enable" {
  type = bool
}

variable "kubernetes" {
  type = string
  default = "k3s"
}

variable "region" {
  type = string
}

variable "zone" {
  type = string
}

variable "project" {
  type = string
}

variable "size" {
  type = string
}

variable "file_key_private" {
  type = string
  sensitive = true
}

variable "public_key" {
  type = string
}

variable "domain" {
  type = string
  default = "example.com"
}

variable "subdomains" {
  type = list(string)
  default = ["*"]
}

variable "kubernetes_config_file" {
  type = string
  sensitive = true
}

variable "k3s_config_file" {
  type = string
}

variable "ssh_key" {
  type = string
}

variable "ssh_user" {
  type = string
}

variable "prefix" {
  type = string
  default = "terraform-cluster"
}
