variable "region" {
  type = string
}

variable "zone" {
  type = string
}

variable "size" {
  type = string
}

variable "node_count" {
  type = string
  default = 1
}

variable "min_nodes" {
  type = string
  default = 1
}

variable "max_nodes" {
  type = string
  default = 1
}

variable "auto_scale" {
  type = bool
  default = false
}

variable "kubernetes_version" {
  type = string
  default = "1.25.4-do.0"
}

variable "kubernetes_config_file" {
  type = string
}
