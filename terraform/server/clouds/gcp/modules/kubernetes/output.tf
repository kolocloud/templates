output "cluster_id" {
  value = google_container_cluster.cluster.id
}

output "cluster_ip" {
  value = google_container_cluster.cluster.endpoint
}
