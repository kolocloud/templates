resource "google_compute_network" "custom" {
  name = "terraform-network-kubernetes"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "custom" {
  name          = "terraform-subnetwork-kubernetes"
  ip_cidr_range = "10.2.0.0/16"
  region        = var.region
  network       = google_compute_network.custom.id

  secondary_ip_range {
    range_name    = "tf-test-secondary-range-update1"
    ip_cidr_range = "192.168.10.0/24"
  }
}

resource "google_container_cluster" "cluster" {
  name               = "cluster"
  location           = var.zone
  initial_node_count = var.node_count

  remove_default_node_pool = true

#  master_version = var.kubernetes_version

  network    = google_compute_network.custom.id
  subnetwork = google_compute_subnetwork.custom.id
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "my-node-pool"
  location   = var.zone
  cluster    = google_container_cluster.cluster.name
  node_count = var.node_count

  node_config {
    preemptible  = true
    machine_type = var.size
    disk_size_gb = 10
  }
}

#resource "local_file" "kubeconfig" {
#  content    = google_container_cluster.cluster.
#  filename   = pathexpand(var.kubernetes_config_file)
#  depends_on = [google_container_cluster.cluster]
#}