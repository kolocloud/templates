variable "domain" {
  type = string
}

variable "region" {
  type = string
}

variable "size" {
  type = string
}

variable "file_key_private" {
  type = string
  sensitive = true
}

variable "public_key" {
  type = string
  sensitive = true
}

variable "ssh_key" {
  type = string
  sensitive = true
}

variable "prefix" {
  type = string
  default = "terraform-cluster"
}
