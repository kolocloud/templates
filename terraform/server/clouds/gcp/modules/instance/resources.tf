resource "google_compute_network" "vpc_network" {
  name = "${var.prefix}-network"
}

resource "google_compute_address" "static" {
  name = "${var.prefix}-ipv4-address"
}

resource "google_compute_firewall" "ssh-rule" {
  name = "${var.prefix}-ssh"
  network = google_compute_network.vpc_network.name
  allow {
    protocol = "tcp"
    ports = ["22"]
  }
  target_tags = ["vm-instance"]
  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "allow_tls" {
  name = "${var.prefix}-allow-tls"
  network = google_compute_network.vpc_network.name
  allow {
    protocol = "tcp"
    ports = ["443"]
  }
  target_tags = ["vm-instance"]
  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "allow_tls_kubernetes" {
  name = "${var.prefix}-allow-tls-kubernetes"
  network = google_compute_network.vpc_network.name
  allow {
    protocol = "tcp"
    ports = ["6443"]
  }
  target_tags = ["vm-instance"]
  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_instance" "vm_instance" {
  name         = "cluster"
  machine_type = var.size
  tags         = ["vm-instance"]
  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  metadata = {
    ssh-keys = "${var.ssh_key}"
  }

  network_interface {
    network = google_compute_network.vpc_network.name
    access_config {
      nat_ip = google_compute_address.static.address
    }
  }
}