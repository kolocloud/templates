#output "cluster_id" {
#  value = aws_instance.app_server.id
#}

output "cluster_ip" {
  value = google_compute_instance.vm_instance.network_interface.0.access_config.0.nat_ip
}

output "cluster_ip_private" {
  value = google_compute_instance.vm_instance.network_interface.0.access_config.0.nat_ip
}
