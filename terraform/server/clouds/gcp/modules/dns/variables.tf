variable "domain" {
  type = string
}

variable "target_ips" {
  type = list(string)
}

variable "subdomains" {
  type = list(string)
}
