resource "google_dns_record_set" "domain" {
  for_each = toset(var.subdomains)
  name = "${each.value}.${var.domain}."
  type = "A"
  ttl  = 60

  managed_zone = "prod-zone"
  rrdatas = var.target_ips
}
