module "instance" {
  count = var.kubernetes == "k3s" ? 1 : 0

  source    = "./modules/instance"
  domain = var.domain
  region = var.region
  size = var.size
  file_key_private = var.file_key_private
  public_key = var.public_key
  ssh_key = var.ssh_key
  prefix = var.prefix
}

module "dns-instance" {
  count = var.dns_enable && var.kubernetes == "k3s" ? 1 : 0

  source    = "./modules/dns"
  domain = var.domain
  subdomains = var.subdomains

  target_ips = [module.instance[0].cluster_ip]

  depends_on = [
    module.instance
  ]
}

module "dns-kubernetes" {
  count = var.dns_enable && var.kubernetes == "managed" ? 1 : 0

  source    = "./modules/dns"
  domain = var.domain
  subdomains = var.subdomains

  target_ips = [module.kubernetes[0].cluster_ip]

  depends_on = [
    module.kubernetes
  ]
}

module "kubernetes" {
  count = var.kubernetes == "managed" ? 1 : 0

  source    = "./modules/kubernetes"
  region = var.region
  zone = var.zone
  size = var.size
  node_count = 1

  min_nodes = 1
  max_nodes = 1
  auto_scale = false

  kubernetes_config_file = var.kubernetes_config_file

  kubernetes_version = "1.25.4"
}

module "k3s" {
  count = var.kubernetes == "k3s" ? 1 : 0

  source    = "../../modules/k3s"
  ssh_user = var.ssh_user

  file_key_private = var.file_key_private
  kubernetes_config_file = var.kubernetes_config_file
  k3s_config_file = var.k3s_config_file

  ip_public = module.instance[0].cluster_ip
  ip_private = module.instance[0].cluster_ip_private

  depends_on = [
    module.instance
  ]
}
