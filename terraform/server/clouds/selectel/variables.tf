variable "token" {
  type = string
  sensitive = true
}

variable "domain_name" {
  type = string
  sensitive = true
}

variable "tenant_id" {
  type = string
  sensitive = true
}

variable "cores" {
  type = string
}

variable "memory" {
  type = string
}

variable "size" {
  type = string
}

variable "user_name" {
  type = string
  sensitive = true
}

variable "password" {
  type = string
  sensitive = true
}

variable "domain" {
  type = string
  default = "example.com"
}

variable "subdomains" {
  type = list(string)
  default = ["*"]
}

variable "kubernetes_config_file" {
  type = string
  sensitive = true
}

variable "k3s_config_file" {
  type = string
}

variable "file_key_private" {
  type = string
  sensitive = true
}

variable "public_key" {
  type = string
}

variable "ssh_user" {
  type = string
}

variable "region" {
  default = "ru-3"
}

variable "az_zone" {
  default = "ru-3b"
}

variable "volume_type" {
  default = "fast.ru-3b"
}

variable "subnet_cidr" {
  default = "10.10.0.0/24"
}

variable "prefix" {
  type = string
  default = "terraform-cluster"
}
