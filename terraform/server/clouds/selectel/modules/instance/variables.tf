variable "domain" {
  type = string
}

variable "cores" {
  type = string
}

variable "memory" {
  type = string
}

variable "size" {
  type = string
}

variable "region" {
  type = string
}

variable "az_zone" {
  type = string
}

variable "volume_type" {
  type = string
}

variable "subnet_cidr" {
  type = string
}

variable "public_key" {
  type = string
  sensitive = true
}

variable "prefix" {
  type = string
  default = "terraform-cluster"
}
