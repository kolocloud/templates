# Creating the SSH key
resource "openstack_compute_keypair_v2" "key_tf" {
  name       = "${var.prefix}-key_tf"
  region     = var.region
  public_key = var.public_key
}

# Request external-network ID by name
data "openstack_networking_network_v2" "external_net" {
  name = "external-network"
}

# Creating a router
resource "openstack_networking_router_v2" "router_tf" {
  name                = "${var.prefix}-router_tf"
  external_network_id = data.openstack_networking_network_v2.external_net.id
}

# Creating a network
resource "openstack_networking_network_v2" "network_tf" {
  name = "${var.prefix}-network_tf"
}

# Creating a subnet
resource "openstack_networking_subnet_v2" "subnet_tf" {
  network_id = openstack_networking_network_v2.network_tf.id
  name       = "${var.prefix}-subnet_tf"
  cidr       = var.subnet_cidr
}

# Connecting the router to the subnet
resource "openstack_networking_router_interface_v2" "router_interface_tf" {
  router_id = openstack_networking_router_v2.router_tf.id
  subnet_id = openstack_networking_subnet_v2.subnet_tf.id
}

# Searching for the image ID (that the server will be created from) by its name
data "openstack_images_image_v2" "ubuntu_image" {
  most_recent = true
  visibility  = "public"
  name        = "Ubuntu 20.04 LTS 64-bit"
}

# Creating a unique flavor name
resource "random_string" "random_name_server" {
  length  = 16
  special = false
}

# Creating a server configuration with 1 vCPU and 1 GB RAM
# Parameter  disk = 0  makes the network volume a boot one
resource "openstack_compute_flavor_v2" "flavor_server" {
  name      = "${var.prefix}-server-${random_string.random_name_server.result}"
  ram       = var.memory
  vcpus     = var.cores
  disk      = "0"
  is_public = "false"
}

# Creating a 5 GB network boot volume from the image
resource "openstack_blockstorage_volume_v3" "volume_server" {
  name                 = "${var.prefix}-volume-for-server1"
  size                 = var.size
  image_id             = data.openstack_images_image_v2.ubuntu_image.id
  volume_type          = var.volume_type
  availability_zone    = var.az_zone
  enable_online_resize = true
  lifecycle {
    ignore_changes = [image_id]
  }
}

# Creating a server
resource "openstack_compute_instance_v2" "server_tf" {
  name              = "server_tf"
  flavor_id         = openstack_compute_flavor_v2.flavor_server.id
  key_pair          = openstack_compute_keypair_v2.key_tf.id
  availability_zone = var.az_zone
  network {
    uuid = openstack_networking_network_v2.network_tf.id
  }
  block_device {
    uuid             = openstack_blockstorage_volume_v3.volume_server.id
    source_type      = "volume"
    destination_type = "volume"
    boot_index       = 0
  }
  vendor_options {
    ignore_resize_confirmation = true
  }
  lifecycle {
    ignore_changes = [image_id]
  }
}

# Creating a floating IP
resource "openstack_networking_floatingip_v2" "fip_tf" {
  pool = "external-network"
}

# Associating the floating IP to the server
resource "openstack_compute_floatingip_associate_v2" "fip_tf" {
  floating_ip = openstack_networking_floatingip_v2.fip_tf.address
  instance_id = openstack_compute_instance_v2.server_tf.id
}
