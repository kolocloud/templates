resource "selectel_domains_domain_v1" "domain_1" {
  name = var.domain
}

resource "selectel_domains_record_v1" "a_record_1" {
  for_each = toset(var.subdomains)
  name    = "${each.value}.${var.domain}"
  domain_id = selectel_domains_domain_v1.domain_1.id
  type = "A"
  ttl = 60
  content = var.target_ip
}
