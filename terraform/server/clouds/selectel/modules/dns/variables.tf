variable "domain" {
  type = string
}

variable "target_ip" {
  type = string
}

variable "subdomains" {
  type = list(string)
}
