module "instance" {
  source    = "./modules/instance"
  domain = var.domain
  az_zone = var.az_zone
  size = var.size

  cores = var.cores
  memory = var.memory
  public_key = var.public_key
  region = var.region
  volume_type = var.volume_type
  subnet_cidr = var.subnet_cidr
}

module "dns-instance" {
  source    = "./modules/dns"
  domain = var.domain
  subdomains = var.subdomains

  target_ip = module.instance.cluster_ip

  depends_on = [
    module.instance
  ]
}

module "k3s" {
  source    = "../../modules/k3s"
  ssh_user = var.ssh_user

  file_key_private = var.file_key_private
  kubernetes_config_file = var.kubernetes_config_file

  ip_public = module.instance.cluster_ip
  ip_private = module.instance.cluster_ip

  k3s_config_file = var.k3s_config_file

  depends_on = [
    module.instance
  ]
}
