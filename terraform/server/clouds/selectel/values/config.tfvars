file_key_private = "~/.ssh/id_rsa_cloud"
kubernetes_config_file = "~/.k3s/config"

k3s_config_file = "/home/k3s.yaml"

cores = 2
memory = 2048
size = 10

ssh_user = "root"
