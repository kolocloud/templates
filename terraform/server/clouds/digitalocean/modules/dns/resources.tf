resource "digitalocean_domain" "default" {
  name = var.domain
  ip_address = var.target_ip
}

resource "digitalocean_record" "a" {
  for_each = toset(var.subdomains)
  name    = "${each.value}.${var.domain}."
  domain = digitalocean_domain.default.id
  type   = "A"
  value  = var.target_ip
  ttl = 60
}
