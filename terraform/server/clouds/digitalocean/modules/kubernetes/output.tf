output "cluster_id" {
  value = digitalocean_kubernetes_cluster.cluster.id
}

output "cluster_ip" {
  value = digitalocean_kubernetes_cluster.cluster.ipv4_address
}
