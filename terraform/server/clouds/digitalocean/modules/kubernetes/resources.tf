resource "digitalocean_kubernetes_cluster" "cluster" {
  name    = "cluster"
  region  = var.region
  version = var.kubernetes_version

  node_pool {
    name       = "autoscale-worker-pool"
    size       = var.size
    auto_scale = var.auto_scale
    node_count = var.node_count
    min_nodes  = var.min_nodes
    max_nodes  = var.min_nodes
  }
}

resource "local_file" "kubeconfig" {
  content    = digitalocean_kubernetes_cluster.cluster.kube_config[0].raw_config
  filename   = pathexpand(var.kubernetes_config_file)
  depends_on = [digitalocean_kubernetes_cluster.cluster]
}
