output "cluster_id" {
  value = digitalocean_droplet.cluster.id
}

output "cluster_ip" {
  value = digitalocean_droplet.cluster.ipv4_address
}

output "cluster_ip_private" {
  value = digitalocean_droplet.cluster.ipv4_address_private
}
