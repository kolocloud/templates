variable "domain" {
  type = string
}

variable "region" {
  type = string
}

variable "size" {
  type = string
}

variable "file_key_private" {
  type = string
  sensitive = true
}
