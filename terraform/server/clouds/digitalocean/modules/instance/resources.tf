data "digitalocean_ssh_key" "terraform" {
  name = "terraform"
}

resource "digitalocean_droplet" "cluster" {
  image    = "ubuntu-20-04-x64"
  name     = "cluster"
  region   = var.region
  size     = var.size
  ssh_keys = [
    data.digitalocean_ssh_key.terraform.id
  ]

  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.file_key_private)
    timeout = "2m"
  }
}
