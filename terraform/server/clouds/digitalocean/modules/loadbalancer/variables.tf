variable "domain" {
  type = string
}

variable "region" {
  type = string
}

variable "droplet_ids" {
  type = list(string)
}
