resource "digitalocean_certificate" "cert" {
  name             = "cert"
  private_key      = file("cert/privkey.pem")
  leaf_certificate  = file("cert/cert.pem")
  certificate_chain = file("cert/fullchain.pem")

  lifecycle {
    create_before_destroy = true
  }
}

resource "digitalocean_loadbalancer" "public" {
  name   = "loadbalancer-1"
  region   = var.region

  forwarding_rule {
    entry_port     = 443
    entry_protocol = "https"

    target_port     = 80
    target_protocol = "http"

    certificate_name = digitalocean_certificate.cert.name
  }

  healthcheck {
    port     = 22
    protocol = "tcp"
  }

  droplet_ids = var.droplet_ids
}
