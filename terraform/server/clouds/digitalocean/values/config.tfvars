file_key_private = "~/.ssh/id_rsa_cloud"
kubernetes_config_file = "~/.k3s/config"

k3s_config_file = "/home/k3s.yaml"

ssh_user = "root"
region = "fra1"
size = "s-2vcpu-4gb"

load_balancer_enable = false
dns_enable = true
kubernetes = "k3s"
