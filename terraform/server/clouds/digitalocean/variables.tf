variable "load_balancer_enable" {
  type = bool
}

variable "dns_enable" {
  type = bool
}

variable "kubernetes" {
  type = string
  default = "k3s"
}

variable "region" {
  type = string
}

variable "size" {
  type = string
}

variable "token" {
  type = string
  sensitive = true
}

variable "file_key_private" {
  type = string
  sensitive = true
}

variable "domain" {
  type = string
  default = "example.com"
}

variable "subdomains" {
  type = list(string)
  default = ["*"]
}

variable "kubernetes_config_file" {
  type = string
}

variable "ssh_key" {
  type = string
}

variable "k3s_config_file" {
  type = string
}

variable "ssh_user" {
  type = string
}

variable "kubernetes_lb_ip" {
  type = string
  default = "0.0.0.0"
}
