module "instance" {
  count = var.kubernetes == "k3s" ? 1 : 0

  source    = "./modules/instance"
  domain = var.domain
  region = var.region
  size = var.size
  file_key_private = var.file_key_private
}

module "k3s" {
  count = var.kubernetes == "k3s" ? 1 : 0

  source    = "../../modules/k3s"
  ssh_user = var.ssh_user

  file_key_private = var.file_key_private
  kubernetes_config_file = var.kubernetes_config_file

  ip_public = module.instance.0.cluster_ip
  ip_private = module.instance.0.cluster_ip_private

  k3s_config_file = var.k3s_config_file

  depends_on = [
    module.instance
  ]
}

module "kubernetes" {
  count = var.kubernetes == "managed" ? 1 : 0

  source    = "./modules/kubernetes"
  region = var.region
  size = var.size
  node_count = 1
  min_nodes = 1
  max_nodes = 1
  auto_scale = false

  kubernetes_config_file = var.kubernetes_config_file

  kubernetes_version = "1.25.4-do.0"
}

module "loadbalancer" {
  count = var.load_balancer_enable ? 1 : 0

  source    = "./modules/loadbalancer"
  domain = var.domain
  region = var.region

  droplet_ids = [module.instance.0.cluster_id]

  depends_on = [
    module.instance
  ]
}

module "dns-instance" {
  count = var.dns_enable && var.kubernetes == "k3s" && !var.load_balancer_enable ? 1 : 0

  source    = "./modules/dns"
  domain = var.domain
  subdomains = var.subdomains

  target_ip = module.instance.0.cluster_ip

  depends_on = [
    module.instance
  ]
}

module "dns-kubernetes" {
  count = var.dns_enable && var.kubernetes == "managed" && !var.load_balancer_enable ? 1 : 0

  source    = "./modules/dns"
  domain = var.domain
  subdomains = var.subdomains

  target_ip = var.kubernetes_lb_ip

  depends_on = [
    module.kubernetes
  ]
}

module "dns-lb" {
  count = var.dns_enable && var.kubernetes == "k3s" && var.load_balancer_enable ? 1 : 0

  source    = "./modules/dns"
  domain = var.domain
  subdomains = var.subdomains

  target_ip = module.loadbalancer[0].ip_public

  depends_on = [
    module.loadbalancer
  ]
}
