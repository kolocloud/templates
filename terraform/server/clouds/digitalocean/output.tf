output "cluster_ip" {
  value = [
    for item in module.instance : item.cluster_ip
  ]
}

output "kubernetes_cluster_ip" {
  value = [
    for item in module.kubernetes : item.cluster_ip
  ]
}

output "loadbalancer_ip" {
  value = [
    for item in module.loadbalancer : item.ip_public
  ]
}
