terraform {
  backend "http" {}

  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
    }
  }
}

provider "digitalocean" {
  token = var.token
}

data "digitalocean_ssh_key" "terraform" {
  name = "terraform"
}