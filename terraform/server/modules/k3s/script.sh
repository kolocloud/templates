curl -sfL https://get.k3s.io | sh -s - --disable traefik

sudo sed -r 's/(\b[0-9]{1,3}\.){3}[0-9]{1,3}\b'/"$1"/ /etc/rancher/k3s/k3s.yaml > /tmp/k3s-tmp.yaml
sudo sed -r 's/(certificate-authority-data)/insecure-skip-tls-verify: true #certificate-authority-data/' /tmp/k3s-tmp.yaml > $2
sudo chmod 644 $2
