resource "null_resource" "k3s-install" {
  provisioner "file" {
    connection {
      type     = "ssh"
      user     = var.ssh_user
      private_key = file(var.file_key_private)
      host     = var.ip_public
    }
    source      = "${path.module}/script.sh"
    destination = "/tmp/script.sh"
  }

  provisioner "remote-exec" {
    connection {
      type     = "ssh"
      user     = var.ssh_user
      private_key = file(var.file_key_private)
      host     = var.ip_public
    }
    inline = [
      "chmod +x /tmp/script.sh",
      "/tmp/script.sh ${var.ip_public} ${var.k3s_config_file}",
    ]
  }
}

resource "null_resource" "k3s-copy-config" {
  triggers = {
    build_number = "${timestamp()}"
  }

  provisioner "local-exec" {
    command = "scp -o StrictHostKeyChecking=no -o ConnectionAttempts=10 -o ConnectTimeout=10 -o UserKnownHostsFile=/dev/null -i ${var.file_key_private} ${var.ssh_user}@${var.ip_public}:${var.k3s_config_file} ${var.kubernetes_config_file}"
  }

  depends_on = [
    null_resource.k3s-install
  ]
}
