variable "ip_public" {
  type = string
}

variable "ip_private" {
  type = string
}

variable "ssh_user" {
  type = string
}

variable "file_key_private" {
  type = string
  sensitive = true
}

variable "kubernetes_config_file" {
  type = string
}

variable "k3s_config_file" {
  type = string
}
