output "private_ip" {
  value = [
    for item in yandex_compute_instance.runner : item.network_interface.0.ip_address
  ]
}

output "public_ip" {
  value = [
    for item in yandex_compute_instance.runner : item.network_interface.0.nat_ip_address
  ]
}

output "ssh_user" {
  value = var.ssh_user
}
