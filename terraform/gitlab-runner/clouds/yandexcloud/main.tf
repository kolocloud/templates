resource "yandex_compute_instance" "runner" {
  count = var.runner_count
  name = "${var.prefix}-${var.runner_group}-${count.index}"

  resources {
    cores  = var.cores
    memory = var.memory
  }

  boot_disk {
    initialize_params {
      # yc compute image list --folder-id standard-images
      # ubuntu-2204-lts
      image_id = "fd89n8278rhueakslujo"
      size     = var.size
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet.id
    nat       = true
  }

  scheduling_policy {
    preemptible = true
  }

  metadata = {
    ssh-keys = var.ssh_key_pub
  }

  allow_stopping_for_update = true
}

resource "yandex_vpc_network" "network" {
  name = "${var.prefix}-network"
}

resource "yandex_vpc_subnet" "subnet" {
  name = "${var.prefix}-subnet"
  zone           = var.zone
  network_id     = yandex_vpc_network.network.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

module "gitlab-runner" {
  count = var.runner_count
  source    = "../../modules/gitlab-runner"

  ssh_user = var.ssh_user
  concurrent = var.concurrent
  ci_token = var.ci_token
  projects = var.projects

  ssh_key = base64decode(var.ssh_key)

  host = yandex_compute_instance.runner[count.index].network_interface.0.nat_ip_address

  depends_on = [
    yandex_compute_instance.runner
  ]
}
