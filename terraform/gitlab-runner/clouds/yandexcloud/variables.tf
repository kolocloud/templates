variable "cloud_id" {
  type = string
}

variable "folder_id" {
  type = string
}

variable "zone" {
  type = string
}

variable "runner_count" {
  type = string
}

variable "runner_group" {
  type = string
  default = "default"
}

variable "concurrent" {
  type = string
}

variable "cores" {
  type = string
}

variable "memory" {
  type = string
}

variable "size" {
  type = string
}

variable "token" {
  type = string
  sensitive = true
}

variable "ssh_key_pub" {
  type = string
  sensitive = true
}

variable "ssh_key" {
  type = string
  sensitive = true
}

variable "ssh_user" {
  type = string
}

variable "ci_token" {
  type = string
}

variable "projects" {
  type = string
}

variable "prefix" {
  type = string
  default = "terraform-runner"
}
