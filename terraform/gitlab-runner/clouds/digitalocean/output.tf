output "public_ip" {
  value = [
    for item in digitalocean_droplet.runner : item.ipv4_address
  ]
}

output "ssh_user" {
  value = var.ssh_user
}
