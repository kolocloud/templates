resource "digitalocean_droplet" "runner" {
  count = var.runner_count

  name     = "gitlab-runner-${var.runner_group}-${count.index}"
  image    = "ubuntu-24-04-x64"
  region   = var.region
  size     = var.size
  ssh_keys = [
    data.digitalocean_ssh_key.terraform.id
  ]

  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = var.ssh_key
    timeout = "2m"
  }
}

module "gitlab-runner" {
  count = var.runner_count
  source    = "../../modules/gitlab-runner"

  ssh_user = var.ssh_user
  concurrent = var.concurrent
  ci_token = var.ci_token
  projects = var.projects

  ssh_key = base64decode(var.ssh_key)

  host = digitalocean_droplet.runner[count.index].ipv4_address

  depends_on = [
    digitalocean_droplet.runner
  ]
}
