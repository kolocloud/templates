output "public_ip" {
  value = [
    for item in google_compute_instance.runner : item.network_interface.0.access_config.0.nat_ip
  ]
}

output "ssh_user" {
  value = var.ssh_user
}
