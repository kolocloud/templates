resource "google_compute_network" "vpc_network" {
  name = "${var.prefix}-network"
}

resource "google_compute_address" "static" {
  count = var.runner_count
  name = "${var.prefix}-ipv4-address-${count.index}"
}

resource "google_compute_firewall" "ssh-rule" {
  name = "${var.prefix}-ssh"
  network = google_compute_network.vpc_network.name
  allow {
    protocol = "tcp"
    ports = ["22"]
  }
  target_tags = ["vm-instance"]
  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_instance" "runner" {
  count = var.runner_count

  name         = "gitlab-runner-${var.runner_group}-${count.index}"
  machine_type = var.size
  tags         = ["vm-instance"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-12"
    }
  }

  metadata = {
    ssh-keys = var.ssh_key_pub
  }

  network_interface {
    network = google_compute_network.vpc_network.name
    access_config {
      nat_ip = google_compute_address.static[count.index].address
    }
  }

  allow_stopping_for_update = true
}

module "gitlab-runner" {
  count = var.runner_count
  source    = "../../modules/gitlab-runner"

  ssh_user = var.ssh_user
  concurrent = var.concurrent
  ci_token = var.ci_token
  projects = var.projects

  ssh_key = base64decode(var.ssh_key)

  host = google_compute_instance.runner[count.index].network_interface.0.access_config.0.nat_ip

  depends_on = [
    google_compute_instance.runner
  ]
}
