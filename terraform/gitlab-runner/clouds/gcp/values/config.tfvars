ssh_user = "ubuntu"

runner_count = 2
runner_group = "default"
concurrent = 2

project = "majestic-energy-376115"
region = "us-central1"
zone = "us-central1-c"
size = "f1-micro"
