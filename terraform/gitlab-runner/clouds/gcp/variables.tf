variable "concurrent" {
  type = string
}

variable "runner_count" {
  type = string
}

variable "runner_group" {
  type = string
  default = "default"
}

variable "region" {
  type = string
}

variable "zone" {
  type = string
}

variable "project" {
  type = string
}

variable "size" {
  type = string
}

variable "ssh_key_pub" {
  type = string
  sensitive = true
}

variable "ssh_key" {
  type = string
  sensitive = true
}

variable "ssh_user" {
  type = string
}

variable "ci_token" {
  type = string
}

variable "projects" {
  type = string
}

variable "prefix" {
  type = string
  default = "terraform-runner"
}
