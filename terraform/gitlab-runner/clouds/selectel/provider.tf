terraform {
  backend "http" {}

  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.54"
    }
    selectel = {
      source  = "selectel/selectel"
      version = "~> 5.1"
    }
  }
}

provider "openstack" {
  auth_url    = "https://api.selvpc.ru/identity/v3"
  domain_name = var.domain_name
  tenant_id   = var.tenant_id
  user_name   = var.user_name
  password    = var.password
  region      = var.region
}

provider "selectel" {
  token       = var.token
  domain_name = var.domain_name
  username    = var.user_name
  password    = var.password
}
