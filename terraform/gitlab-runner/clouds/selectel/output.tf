output "public_ip" {
  value = [
    for item in openstack_networking_floatingip_v2.fip_tf : item.address
  ]
}

output "ssh_user" {
  value = var.ssh_user
}
