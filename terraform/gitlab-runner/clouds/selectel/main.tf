# Creating the SSH key
resource "openstack_compute_keypair_v2" "key_tf" {
  name       = "${var.prefix}-key"
  region     = var.region
  public_key = var.ssh_key_pub
}

# Request external-network ID by name
data "openstack_networking_network_v2" "external_net" {
  name = "external-network"
}

# Creating a router
resource "openstack_networking_router_v2" "router_tf" {
  name                = "${var.prefix}-router_tf"
  external_network_id = data.openstack_networking_network_v2.external_net.id
}

# Creating a network
resource "openstack_networking_network_v2" "network_tf" {
  name = "${var.prefix}-network"
}

# Creating a subnet
resource "openstack_networking_subnet_v2" "subnet_tf" {
  network_id = openstack_networking_network_v2.network_tf.id
  name       = "${var.prefix}-subnet"
  cidr       = var.subnet_cidr
}

# Connecting the router to the subnet
resource "openstack_networking_router_interface_v2" "router_interface_tf" {
  router_id = openstack_networking_router_v2.router_tf.id
  subnet_id = openstack_networking_subnet_v2.subnet_tf.id
}

# Searching for the image ID (that the server will be created from) by its name
data "openstack_images_image_v2" "ubuntu_image" {
  most_recent = true
  visibility  = "public"
  name        = "Ubuntu 22.04 LTS 64-bit"
}

# Creating a unique flavor name
resource "random_string" "random_name_server" {
  length  = 16
  special = false
}

# Creating a server configuration with 1 vCPU and 1 GB RAM
# Parameter  disk = 0  makes the network volume a boot one
resource "openstack_compute_flavor_v2" "flavor_server" {
  name      = "${var.prefix}-server-${random_string.random_name_server.result}"
  ram       = var.memory
  vcpus     = var.cores
  disk      = "0"
  is_public = "false"
}

# Creating a 5 GB network boot volume from the image
resource "openstack_blockstorage_volume_v3" "volume_server" {
  count = var.runner_count

  name                 = "${var.prefix}-volume-for-${var.runner_group}-${count.index}"
  size                 = var.size
  image_id             = data.openstack_images_image_v2.ubuntu_image.id
  volume_type          = var.volume_type
  availability_zone    = var.az_zone
  enable_online_resize = true
  lifecycle {
    ignore_changes = [image_id]
  }
}

# Creating a server
resource "openstack_compute_instance_v2" "runner" {
  count = var.runner_count

  name              = "${var.prefix}-${var.runner_group}-${count.index}"
  flavor_id         = openstack_compute_flavor_v2.flavor_server.id
  key_pair          = openstack_compute_keypair_v2.key_tf.id
  availability_zone = var.az_zone
  network {
    uuid = openstack_networking_network_v2.network_tf.id
  }
  block_device {
    uuid             = openstack_blockstorage_volume_v3.volume_server[count.index].id
    source_type      = "volume"
    destination_type = "volume"
    boot_index       = 0
  }
  vendor_options {
    ignore_resize_confirmation = true
  }
  lifecycle {
    ignore_changes = [image_id]
  }
}

# Creating a floating IP
resource "openstack_networking_floatingip_v2" "fip_tf" {
  count = var.runner_count
  pool = "external-network"
}

# Associating the floating IP to the server
resource "openstack_compute_floatingip_associate_v2" "fip_tf" {
  count = var.runner_count
  floating_ip = openstack_networking_floatingip_v2.fip_tf[count.index].address
  instance_id = openstack_compute_instance_v2.runner[count.index].id
}

module "gitlab-runner" {
  count = var.runner_count
  source    = "../../modules/gitlab-runner"

  ssh_user = var.ssh_user
  concurrent = var.concurrent
  ci_token = var.ci_token
  projects = var.projects

  ssh_key = base64decode(var.ssh_key)

  host = openstack_networking_floatingip_v2.fip_tf[count.index].address

  depends_on = [
    openstack_compute_instance_v2.runner
  ]
}
