variable "token" {
  type = string
  sensitive = true
}

variable "domain_name" {
  type = string
  sensitive = true
}

variable "tenant_id" {
  type = string
  sensitive = true
}

variable "user_name" {
  type = string
  sensitive = true
}

variable "password" {
  type = string
  sensitive = true
}

variable "concurrent" {
  type = string
}

variable "cores" {
  type = string
}

variable "memory" {
  type = string
}

variable "size" {
  type = string
}

variable "region" {
  default = "ru-3"
}

variable "az_zone" {
  default = "ru-3b"
}

variable "volume_type" {
  default = "fast.ru-3b"
}

variable "subnet_cidr" {
  default = "10.10.0.0/24"
}

variable "runner_count" {
  type = string
}

variable "runner_group" {
  type = string
  default = "default"
}

variable "ssh_key_pub" {
  type = string
  sensitive = true
}

variable "ssh_key" {
  type = string
  sensitive = true
}

variable "ssh_user" {
  type = string
}

variable "ci_token" {
  type = string
}

variable "projects" {
  type = string
}

variable "prefix" {
  type = string
  default = "terraform-runner"
}
