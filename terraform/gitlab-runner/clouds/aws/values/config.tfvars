ssh_user = "admin"

runner_count = 2
runner_group = "default"
concurrent = 2

region = "us-east-1"
size = "t2.micro"
