resource "aws_key_pair" "terraform" {
  key_name   = "${var.prefix}-key"
  public_key = var.ssh_key_pub
}

resource "aws_security_group" "port_22_ingress" {
  name = "${var.prefix}-port_22_ingress"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_tls" {
  name        = "${var.prefix}-allow_tls"
  description = "Allow TLS inbound traffic"

  ingress {
    description      = "TLS from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}

resource "aws_instance" "runner" {
  count = var.runner_count

  # Debian 12 (HVM), SSD Volume Type / ami-058bd2d568351da34 (64-bit (x86))
  ami           = "ami-058bd2d568351da34"
  instance_type = var.size
  key_name = "terraform-runner-key"

  security_groups = [
    "default",
    aws_security_group.port_22_ingress.name,
    aws_security_group.allow_tls.name,
  ]

  tags = {
    Name = "gitlab-runner-${var.runner_group}-${count.index}"
  }
}

module "gitlab-runner" {
  count = var.runner_count
  source    = "../../modules/gitlab-runner"

  ssh_user = var.ssh_user
  concurrent = var.concurrent
  ci_token = var.ci_token
  projects = var.projects

  ssh_key = base64decode(var.ssh_key)

  host = aws_instance.runner[count.index].public_ip

  depends_on = [
    aws_instance.runner
  ]
}
