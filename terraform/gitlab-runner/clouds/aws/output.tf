output "public_ip" {
  value = [
    for item in aws_instance.runner : item.public_ip
  ]
}

output "ssh_user" {
  value = var.ssh_user
}
