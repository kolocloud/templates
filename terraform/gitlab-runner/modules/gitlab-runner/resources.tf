resource "null_resource" "gitlab-runner" {
  connection {
      type     = "ssh"
      user     = var.ssh_user
      private_key = var.ssh_key
      host     = var.host
  }

  provisioner "file" {
    source      = "${path.module}/script.sh"
    destination = "/tmp/script.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/script.sh",
      "/tmp/script.sh ${var.ci_token} ${var.projects} ${var.concurrent}",
    ]
  }
}
