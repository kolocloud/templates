#!/bin/bash

sudo apt update
sudo apt install -y git docker.io jq

curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"
sudo dpkg -i gitlab-runner_amd64.deb

sudo sed -i "s/concurrent.*/concurrent = $3/" /etc/gitlab-runner/config.toml
sudo gitlab-runner start

IFS=',' read -r -a projects <<< "$2"
for project in "${projects[@]}"
do
  export GITLAB_TOKEN=$(curl -s --request POST \
    --header "PRIVATE-TOKEN: $1" \
    --data "runner_type=project_type&project_id=$project" https://gitlab.com/api/v4/user/runners \
    | jq -r .token \
  )

  sudo gitlab-runner register --url https://gitlab.com/ \
    --token "$GITLAB_TOKEN" \
    --name "my-runner" \
    --executor "docker" \
    --docker-image "alpine" \
    --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
    --docker-volumes /cache \
    --non-interactive
done
