variable "concurrent" {
  type = string
}

variable "host" {
  type = string
}

variable "ssh_user" {
  type = string
}

variable "ssh_key" {
  type = string
  sensitive = true
}

variable "ci_token" {
  type = string
}

variable "projects" {
  type = string
}
