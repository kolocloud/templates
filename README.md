# Kolo Cloud templates

## Initialize
Past into .gitlab-ci.yaml
```yaml
include:
  - project: 'kolocloud/templates'
    ref: 0.0.20
    file: '/templates/app/main.yaml'
```

## Update submodule
```
git submodule add -b main --force https://gitlab.com/kolocloud/templates.git deployments
git submodule update --remote
```

## Features
- [Registry](docs/registry.md)
- [Server](docs/server.md)
