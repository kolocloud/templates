## Install submodule kolocloud/templates
```
git submodule add -b main --force https://gitlab.com/kolocloud/templates.git deployments
git submodule update --remote
```

## Install sops
### Install
```
wget https://github.com/mozilla/sops/releases/download/v3.7.3/sops-v3.7.3.linux.amd64 -O /usr/bin/sops
chmod 755 /usr/bin/sops
```

### Generate key
```
export SOPS_AGE_KEY_FILE=~/my_sops_key.txt
age-keygen -o $SOPS_AGE_KEY_FILE
export AGE_PUBLIC_KEY=age***
```

## Create domain certificate
### Configure DNS Servers
#### Digital ocean
Add DNS servers for your domain.
```
ns1.digitalocean.com
ns2.digitalocean.com
ns3.digitalocean.com
```

#### Yandex cloud
Add DNS servers for your domain.
```
ns1.yandexcloud.net
ns2.yandexcloud.net
```

#### Selectel
Add DNS servers for your domain.
```
ns1.selectel.org
ns2.selectel.org
ns3.selectel.org
ns4.selectel.org
```

#### GCP
Run terraform
```
export TF_CONFIG=$(pwd)/config/dns/gcp
cd deployments/terraform/dns/gcp
mkdir credentials
touch credentials/service-account.json
terraform init -backend-config=$TF_CONFIG/config/backend.conf
terraform apply -auto-approve -var-file=$TF_CONFIG/values/config.tfvars -var-file=$TF_CONFIG/values/secrets.tfvars -var="domain=example.com"
```

Add created DNS servers to your domain.
```
ns-cloud-b1.googledomains.com
ns-cloud-b2.googledomains.com
ns-cloud-b3.googledomains.com
ns-cloud-b4.googledomains.com
```

#### AWS
Run terraform
```
export TF_CONFIG=$(pwd)/config/dns/aws
cd deployments/terraform/dns/aws
terraform init -backend-config=$TF_CONFIG/config/backend.conf
terraform apply -auto-approve -var-file=$TF_CONFIG/values/config.tfvars -var-file=$TF_CONFIG/values/secrets.tfvars -var="domain=example.com"
```

Add created DNS servers to your domain and read zone_id.
```
ns-1489.awsdns-58.org
ns-1779.awsdns-30.co.uk
ns-247.awsdns-30.com
ns-598.awsdns-10.net
```


### Create certificate
```
export DOMAIN=example.com
dig -t ns $DOMAIN

export CERTBOT_PATH=$(pwd)
certbot certonly --manual -d "*.$DOMAIN" \
    --agree-tos --no-eff-email \
    -m example@mail.com  \
    --logs-dir certbot \
    --config-dir certbot \
    --work-dir certbot
dig -t txt "_acme-challenge.$DOMAIN"
```

### Move certificates
```
mkdir -p cert/$DOMAIN/
cp $CERTBOT_PATH/certbot/live/$DOMAIN/* cert/$DOMAIN/
```

#### Encrypt certificate
```
sops --encrypt --age $AGE_PUBLIC_KEY cert/$DOMAIN/cert.pem > cert/$DOMAIN/cert.pem.enc
sops --encrypt --age $AGE_PUBLIC_KEY cert/$DOMAIN/fullchain.pem > cert/$DOMAIN/fullchain.pem.enc
sops --encrypt --age $AGE_PUBLIC_KEY cert/$DOMAIN/privkey.pem > cert/$DOMAIN/privkey.pem.enc
```

#### Decrypt certificate
```
sops --decrypt cert/$DOMAIN/cert.pem.enc > cert/$DOMAIN/cert.pem
sops --decrypt cert/$DOMAIN/privkey.pem.enc > cert/$DOMAIN/privkey.pem
sops --decrypt cert/$DOMAIN/fullchain.pem.enc > cert/$DOMAIN/fullchain.pem
```

## Configure terraform
### Encrypt secrets
#### Server
```
export CLOUD=yandexcloud
cd config/server/$CLOUD/
sops --encrypt --age $AGE_PUBLIC_KEY config/backend.conf > config/backend.conf.enc
sops --encrypt --age $AGE_PUBLIC_KEY values/secrets.tfvars > values/secrets.tfvars.enc
```

#### Production
```
export CLOUD=yandexcloud
cd config/production/$CLOUD/
sops --encrypt --age $AGE_PUBLIC_KEY config/backend.conf > config/backend.conf.enc
sops --encrypt --age $AGE_PUBLIC_KEY values/secrets.tfvars > values/secrets.tfvars.enc
```

#### Helm
```
cd config/helm/
sops --encrypt --age $AGE_PUBLIC_KEY monitoring/secrets.yaml > monitoring/secrets.yaml.enc
sops --encrypt --age $AGE_PUBLIC_KEY storage/secrets.yaml > storage/secrets.yaml.enc
sops --encrypt --age $AGE_PUBLIC_KEY tracing/secrets.yaml > tracing/secrets.yaml.enc
sops --encrypt --age $AGE_PUBLIC_KEY production/secrets.yaml > production/secrets.yaml.enc
```

### Decrypt secrets
#### Server
```
export CLOUD=yandexcloud
cd config/server/$CLOUD/
sops --decrypt config/backend.conf.enc > config/backend.conf
sops --decrypt values/secrets.tfvars.enc > values/secrets.tfvars
```

#### Production
```
export CLOUD=yandexcloud
cd config/production/$CLOUD/
sops --decrypt config/backend.conf.enc > config/backend.conf
sops --decrypt values/secrets.tfvars.enc > values/secrets.tfvars
```

#### Helm
```
cd config/helm/
sops --decrypt monitoring/secrets.yaml.enc > monitoring/secrets.yaml
sops --decrypt storage/secrets.yaml.enc > storage/secrets.yaml
sops --decrypt tracing/secrets.yaml.enc > tracing/secrets.yaml
sops --decrypt production/secrets.yaml.enc > production/secrets.yaml
```

### Apply
```
export DOMAIN=example.com
export CLOUD=yandexcloud
export ROOT_PATH=$(pwd)
export CI_PROJECT_DIR=$ROOT_PATH

cd $ROOT_PATH/deployments/terraform/server/clouds/$CLOUD
terraform init -backend-config="$ROOT_PATH/config/server/$CLOUD/config/backend.conf"

terraform plan -var-file="$ROOT_PATH/config/server/$CLOUD/values/config.tfvars" \
    -var-file="$ROOT_PATH/config/server/$CLOUD/values/secrets.tfvars" \
    -var="domain=$DOMAIN"

terraform apply --auto-approve -var-file="$ROOT_PATH/config/server/$CLOUD/values/config.tfvars" \
    -var-file="$ROOT_PATH/config/server/$CLOUD/values/secrets.tfvars" \
    -var="domain=$DOMAIN"
```

### Read kubernetes config
```
cat $CI_PROJECT_DIR/kubernetes_config.yaml
export KUBECONFIG=$CI_PROJECT_DIR/kubernetes_config.yaml
kubectl get nodes
```

## Kubernetes
### Copy certs
```
ln -sfn ${ROOT_PATH}/cert/${DOMAIN} ${ROOT_PATH}/deployments/helm/production/cert
ln -sfn ${ROOT_PATH}/cert/${DOMAIN} ${ROOT_PATH}/deployments/helm/monitoring/cert
ln -sfn ${ROOT_PATH}/cert/${DOMAIN} ${ROOT_PATH}/deployments/helm/storage/cert
ln -sfn ${ROOT_PATH}/cert/${DOMAIN} ${ROOT_PATH}/deployments/helm/tracing/cert
```

### Install ingress
```
export HELM_RELEASE=ingress
export HELM_CONFIG=$ROOT_PATH/config/helm/ingress

helm upgrade --install $HELM_RELEASE bitnami/nginx-ingress-controller \
    --values=$HELM_CONFIG/values.yaml
```

### Install gore chart
```
export HELM_RELEASE=gore
export HELM_CONFIG=$ROOT_PATH/config/helm/production

cd $ROOT_PATH/deployments/helm/production

helm upgrade --install $HELM_RELEASE ./ \
    -f $HELM_CONFIG/values.yaml \
    -f $HELM_CONFIG/secrets.yaml \
    --set domain=$DOMAIN \
    --wait
```

### Check tls secrets
```
kubectl get secrets tls -o jsonpath='{.data}'
```

### Test
```
curl https://gateway.$DOMAIN/box/1
```

### Install storage chart
```
export HELM_RELEASE=storage
export HELM_CONFIG=$ROOT_PATH/config/helm/storage

cd $ROOT_PATH/deployments/helm/storage

kubectl create namespace storage
kubens storage

helm upgrade --install $HELM_RELEASE ./ \
    -f $HELM_CONFIG/values.yaml \
    -f $HELM_CONFIG/secrets.yaml \
    --set domain=$DOMAIN \
    --wait
```

#### Test pgadmin chart
```
curl https://pgadmin.$DOMAIN/
```

#### Login pgadmin
Login: chart@domain.com
Password: SuperSecret

#### Read storage secrets
```
kubectl get secrets storage-postgresql -o jsonpath='{.data.postgres-password}' -n storage | base64 --decode
```

### Install monitoring chart
```
export HELM_RELEASE=monitoring
export HELM_CONFIG=$ROOT_PATH/config/helm/monitoring

cd $ROOT_PATH/deployments/helm/monitoring

kubectl create namespace monitoring
kubens monitoring

helm upgrade --install $HELM_RELEASE ./ \
    -f $HELM_CONFIG/values.yaml \
    -f $HELM_CONFIG/secrets.yaml \
    --set domain=$DOMAIN \
    --wait
```

#### Test monitoring chart
```
curl https://prometheus.$DOMAIN/
curl https://grafana.$DOMAIN/
```

#### Login grafana
Login: admin

#### Read password
```
kubectl get secrets monitoring-grafana -o jsonpath='{.data.admin-password}' -n monitoring | base64 -d
```

### Install tracing chart
```
export HELM_RELEASE=tracing
export HELM_CONFIG=$ROOT_PATH/config/helm/tracing

cd $ROOT_PATH/deployments/helm/tracing

kubectl create namespace tracing
kubens tracing

helm upgrade --install $HELM_RELEASE ./ \
    -f $HELM_CONFIG/values.yaml \
    -f $HELM_CONFIG/secrets.yaml \
    --set domain=$DOMAIN \
    --wait
```

#### Test tracing chart
```
curl https://jaeger.$DOMAIN/
```


## Destroy
### Destroy chart
```
helm delete $HELM_RELEASE
```

### Destroy terraform
```
cd $ROOT_PATH/deployments/terraform/server/clouds/$CLOUD
terraform destroy --auto-approve -var-file="$ROOT_PATH/config/server/$CLOUD/values/config.tfvars" \
    -var-file="$ROOT_PATH/config/server/$CLOUD/values/secrets.tfvars" \
    -var="domain=$DOMAIN"
```
