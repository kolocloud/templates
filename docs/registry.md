## Docker build and push image
### Dockerfile
You can use `DOCKERFILE` variable for rewriting name of Dockerfile.

### Initialize
Past into .gitlab-ci.yaml
```yaml
include:
  - project: 'kolocloud/templates'
    ref: 0.0.20
    file: '/templates/app/main.yaml'

stages:
  - build
```

### Vendors
#### Docker Hub
`DOCKERHUB_USER` is name of your dockerhub account https://hub.docker.com/u/kolocloud
`DOCKERHUB_ACCESS_TOKEN` is your Access token. You can create here https://hub.docker.com/settings/security

Set Gitlab CI variables
```shell
DOCKERHUB_USER=kolocloud
DOCKERHUB_ACCESS_TOKEN=dckr_***
```

Past into .gitlab-ci.yaml
```yaml
build:
  stage: build
  variables:
    DOCKER_IMAGE: kolocloud/kolo
    DOCKERFILE: Dockerfile
  extends: .build_docker_image_dockerhub
```

Find your image here: https://hub.docker.com/r/kolocloud/kolo

#### Gitlab
`GITLAB_USERNAME` is name of your token https://gitlab.com/-/profile/personal_access_tokens
`GITLAB_PASSWORD` is your generated access token.

Set Gitlab CI variables
```shell
GITLAB_USERNAME=ci-token
GITLAB_PASSWORD=***
```

Past into .gitlab-ci.yaml
```yaml
build:
  stage: build
  variables:
    DOCKER_IMAGE: registry.gitlab.com/kolocloud/kolo
    DOCKERFILE: Dockerfile
  extends: .build_docker_image_gitlab
```

Find your image here: https://gitlab.com/kolocloud/kolo/container_registry

#### Digital Ocean
`DO_ACCESS_TOKEN` is your generated access token.
You can generate it here: https://cloud.digitalocean.com/account/api/tokens

Set Gitlab CI variables
```shell
DO_ACCESS_TOKEN=dop_*
```

Past into .gitlab-ci.yaml
```yaml
build_digitalocean:
  stage: build
  variables:
    DOCKER_IMAGE: registry.digitalocean.com/mynewreg/alpine
    DOCKERFILE: Dockerfile
  extends: .build_docker_image_digitalocean
```

Find your image here: https://cloud.digitalocean.com/registry


#### Yandex Cloud
`YC_FOLDER_ID` is your folder id.
`YC_SERVICE_ACCOUNT_NAME` is your service account name.
`YC_TOKEN` is your generated access token.
You can create service account and generate access token here: https://console.cloud.yandex.ru/folders/***?section=service-accounts


Set Gitlab CI variables
```shell
YC_FOLDER_ID=
YC_SERVICE_ACCOUNT_NAME=
YC_TOKEN=y0_*
```

Past into .gitlab-ci.yaml
```yaml
build_yandexcloud:
  stage: build
  variables:
    DOCKER_IMAGE: cr.yandex/***/alpine
    DOCKERFILE: Dockerfile
  extends: .build_docker_image_yandexcloud
```

Find your image here: https://console.cloud.yandex.ru/folders/***/container-registry/registries
