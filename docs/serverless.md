# Install

## Initialize
Past into .gitlab-ci.yaml
```yaml
include:
  - project: 'kolocloud/templates'
    ref: 0.0.20
    file: '/templates/app/main.yaml'

stages:
  - build
```

## Vendors
### Google Cloud Platform
#### Code
##### Golang
Use example - [Golang app example](../examples/serverless/gcp/go)

##### Nodejs
Use example - [Nodejs app example](../examples/serverless/gcp/nodejs)

##### Python
Use example - [Python app example](../examples/serverless/gcp/python)

#### Variables
`GCP_PROJECT` is name of your project.
`GCP_REGION` is your region.
`GCP_ZONE` is your zone.
`GCP_SERVICE_ACCOUNT` is your service account.

Set Gitlab CI variables
```shell
GCP_PROJECT=my-project-12345
GCP_REGION=us-central1
GCP_ZONE=us-central1-c
GCP_SERVICE_ACCOUNT=**
```
