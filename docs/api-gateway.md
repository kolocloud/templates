# Install

## Initialize
Enable
https://console.cloud.google.com/apis/library/apigateway.googleapis.com?authuser=3&project=majestic-energy-376115


```shell
export TF_CONFIG=./
terraform init -backend-config=$TF_CONFIG/config/backend.conf
terraform plan -var-file=$TF_CONFIG/values/config.tfvars -var-file=$TF_CONFIG/values/secrets.tfvars -var="domain=example.com"

terraform apply -auto-approve -var-file=$TF_CONFIG/values/config.tfvars -var-file=$TF_CONFIG/values/secrets.tfvars -var="domain=example.com"

```
