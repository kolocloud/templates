current_dir = $(shell pwd)
helm_dir = helm

DOMAIN := $(or ${domain},${domain},)

create-cert-links:
	ln -sfn ${current_dir}/cert/${DOMAIN} ${helm_dir}/production/cert
	ln -sfn ${current_dir}/cert/${DOMAIN} ${helm_dir}/monitoring/cert
	ln -sfn ${current_dir}/cert/${DOMAIN} ${helm_dir}/storage/cert
	ln -sfn ${current_dir}/cert/${DOMAIN} ${helm_dir}/tracing/cert

delete-cert-links:
	rm -r ${helm_dir}/production/cert
	rm -r ${helm_dir}/monitoring/cert
	rm -r ${helm_dir}/storage/cert
	rm -r ${helm_dir}/tracing/cert
