module.exports.handler = function (event, context) {
    let body = {}
    if (event.body) {
        body = JSON.parse(event.body)
    }

    return {
        statusCode: 200,
        body: JSON.stringify(body)
    };
};
