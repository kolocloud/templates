# Runtimes
https://cloud.yandex.com/en/docs/functions/concepts/runtime/

- [Nodejs](#nodejs)
- [Golang](#golang)
- [Python](#python)

<a name="nodejs"></a>
## Nodejs
Available runtimes:
- nodejs10
- nodejs12
- nodejs14
- nodejs16

```yaml
  variables:
    FUNC_NAME: nodejs-func
    FILE_PATH: app
    RUNTIME: nodejs16
    HANDLER: index.handler
```

<a name="golang"></a>
### Golang
Available runtimes:
- golang116
- golang117
- golang118
- golang119

```yaml
  variables:
    FUNC_NAME: go-func
    FILE_PATH: app
    RUNTIME: golang119
    HANDLER: main.Handler
```

<a name="python"></a>
### Python
Available runtimes:
- python37
- python38
- python39
- python311

```yaml
  variables:
    FUNC_NAME: python-func
    FILE_PATH: app
    RUNTIME: python311
    HANDLER: main.handler
```
