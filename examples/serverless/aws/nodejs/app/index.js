exports.handler = async function(event, context, callback) {
    if (event.body) {
        let body = JSON.parse(event.body)
        return JSON.stringify(body)
    }

    return JSON.stringify({})
}
