import json

def handler(event, context):
    d = json.loads(event['body'])
    return {
        'statusCode': 200,
        'body': json.dumps(d)
    }
