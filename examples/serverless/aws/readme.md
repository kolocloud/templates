# Runtimes
https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtimes.html

- [Nodejs](#nodejs)
- [Golang](#golang)
- [Python](#python)

<a name="nodejs"></a>
## Nodejs
Available runtimes:
- nodejs14.x
- nodejs16.x
- nodejs18.x
- nodejs20.x

```yaml
  variables:
    FUNC_NAME: nodejs-func
    FILE_PATH: app
    RUNTIME: nodejs20.x
    HANDLER: index.handler
    BUILD_FILE: function.zip
```

<a name="golang"></a>
### Golang
Available runtimes:
- go1.x

```yaml
  variables:
    FUNC_NAME: go-func
    FILE_PATH: app
    RUNTIME: go1.x
    HANDLER: main
    BUILD_FILE: function.zip
```

<a name="python"></a>
### Python
Available runtimes:
- python3.7
- python3.8
- python3.9
- python3.10
- python3.11

```yaml
  variables:
    FUNC_NAME: python-func
    FILE_PATH: app
    RUNTIME: python3.11
    HANDLER: index.handler
    BUILD_FILE: function.zip
```
