package _go

import (
	"bytes"
	"net/http/httptest"
	"testing"
)

func TestHandler(t *testing.T) {
	jsonBody := []byte(`{"var":"ne"}`)
	bodyReader := bytes.NewReader(jsonBody)

	req := httptest.NewRequest("POST", "/", bodyReader)
	req.Header.Add("Content-Type", "application/json")

	rr := httptest.NewRecorder()
	handler(rr, req)

	want := string(jsonBody)
	if got := rr.Body.String(); got != want {
		t.Errorf("handler() = %q, want %q", got, want)
	}
}
