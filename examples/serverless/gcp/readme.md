# Runtimes
https://cloud.google.com/functions/docs/runtime-support

- [Nodejs](#nodejs)
- [Golang](#golang)
- [Python](#python)

<a name="nodejs"></a>
## Nodejs
Available runtimes:
- nodejs10
- nodejs12
- nodejs14
- nodejs16
- nodejs18
- nodejs20

```yaml
  variables:
    FUNC_NAME: nodejs-func
    FILE_PATH: app
    RUNTIME: nodejs20
    ENTRY_POINT: handler
```

<a name="golang"></a>
### Golang
Available runtimes:
- golang116
- golang117
- golang118
- golang119
- golang120
- golang121

```yaml
  variables:
    FUNC_NAME: go-func
    FILE_PATH: app
    RUNTIME: go119
    ENTRY_POINT: Handler
```

<a name="python"></a>
### Python
Available runtimes:
- python37
- python38
- python39
- python310
- python311
- python312

```yaml
  variables:
    FUNC_NAME: python-func
    FILE_PATH: app
    RUNTIME: python311
    ENTRY_POINT: handler
```
