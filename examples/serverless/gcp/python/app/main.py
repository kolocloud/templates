import functions_framework

@functions_framework.http
def handler(request):
    content_type = request.headers['content-type']
    if content_type == 'application/json':
        request_json = request.get_json(silent=True)
        return request_json
    else:
        raise ValueError("Unknown content type: {}".format(content_type))
