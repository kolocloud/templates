# Storage

## Static
Create static folder for files.
```bash
export CLOUD=yandexcloud
export ROOT_PATH=$(pwd)

mkdir static
ln -sfn $ROOT_PATH/static $ROOT_PATH/deployments/terraform/storage/$CLOUD/static
```

## Clouds
- [AWS](#aws)
- [GCP](#gcp)
- [DO](#do)
- [Yandex Cloud](#yc)

<a name="aws"></a>
### AWS
Example of `config/aws/config.tfvars`

```hcl
region = "us-east-1"
name = "test-bucket-kolocloud"

objects = {
  "index.html" = {
    source       = "static/index.html"
    content_type = "text/html"
  }
}
```

<a name="gcp"></a>
### GCP
Example of `config/gcp/config.tfvars`

```hcl
project = "majestic-energy-376115"
region = "us-central1"
zone = "us-central1-c"
name = "bucket-kolocloud"

objects = {
  "index.html" = {
    source       = "static/index.html"
    content_type = "text/html"
  }
}
```


<a name="do"></a>
### DO
Example of `config/digitalocean/config.tfvars`

```hcl
region = "nyc3"
name = "mynewstoragekolocloud"

objects = {
  "index.html" = {
    source       = "static/index.html"
    content_type = "text/html"
  }
}
```


<a name="yc"></a>
### Yandex Cloud
Example of `config/yandexcloud/config.tfvars`

```hcl
zone = "ru-central1-a"
name = "default-kolocloud"

objects = {
  "index.html" = {
    source       = "static/index.html"
    content_type = "text/html"
  }
}
```
