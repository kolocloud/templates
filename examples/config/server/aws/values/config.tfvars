file_key_private = "~/.ssh/id_rsa_cloud"
kubernetes_config_file = "$CI_PROJECT_DIR/kubernetes_config.yaml"

k3s_config_file = "/home/ec2-user/k3s.yaml"

region = "us-east-1"
size = "t2.micro"
zone_id = "Z0***"

ssh_user = "ec2-user"

elb_enable = false
