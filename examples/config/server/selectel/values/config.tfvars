file_key_private = "~/.ssh/id_rsa_cloud"
kubernetes_config_file = "$CI_PROJECT_DIR/kubernetes_config.yaml"

k3s_config_file = "/home/k3s.yaml"

cores = 2
memory = 2048
size = 10

ssh_user = "root"
