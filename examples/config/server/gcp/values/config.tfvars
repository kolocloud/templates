file_key_private = "~/.ssh/id_rsa_cloud"
kubernetes_config_file = "$CI_PROJECT_DIR/kubernetes_config.yaml"

k3s_config_file = "/home/ubuntu/k3s.yaml"

project = "majestic-energy-000000"
region = "us-central1"
zone = "us-central1-c"
size = "e2-standard-2"

ssh_user = "ubuntu"

dns_enable = true
kubernetes = "k3s"
