file_key_private = "~/.ssh/id_rsa_cloud"
kubernetes_config_file = "$CI_PROJECT_DIR/kubernetes_config.yaml"

k3s_config_file = "/home/ubuntu/k3s.yaml"

zone = "ru-central1-a"
cores = 2
memory = 2
size = 10

ssh_user = "ubuntu"
alb_enable = false
